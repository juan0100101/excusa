<?
$tiempoInicio = microtime();

// creamos una variable para cada elemento enviado por GET
foreach($_GET as $Key => $value) {

  $$Key = utf8_decode($value);

}

if($fecha_ultima_excusa_cargada == -1) {
	
	$fecha_ultima_excusa_cargada = null;
	
}

// incluímos la clase excusa y de paso la conexión a la base de datos
include("../php/MapObjects/excusa.php");

try {

  // traemos una lista de las 15 buenas excusas más recientes
  // tomando en cuenta la fecha límite si la hay
  $sql = 'SELECT *, '
               .'(SELECT COUNT(*) FROM excusas.votos WHERE votos.cod_excusa = excusas.cod_excusa AND votos.calificacion_voto = \'p\') AS arrivotos, '
               .'(SELECT COUNT(*) FROM excusas.votos WHERE votos.cod_excusa = excusas.cod_excusa AND votos.calificacion_voto = \'n\') AS bajivotos '
        .'FROM excusas '
        .'WHERE (SELECT COUNT(*) FROM excusas.votos WHERE votos.cod_excusa = excusas.cod_excusa AND votos.calificacion_voto = \'p\')+3 '
              .' > '
              .'(SELECT COUNT(*) FROM excusas.votos WHERE votos.cod_excusa = excusas.cod_excusa AND votos.calificacion_voto = \'n\') ';
  
  // si hay fecha de última excusa cargada limitamos la búsqueda según ella
  if($fecha_ultima_excusa_cargada) {
  	
    $sql = $sql.'AND excusas.fecha_creacion_excusa < :fecha_ultima_excusa_cargada ';
  	
  }
  
  $sql = $sql.'ORDER BY (arrivotos-bajivotos) DESC, '
             .'         fecha_creacion_excusa DESC '
             .'LIMIT 15';
  
  // conectamos con base de datos  
  $db = conectarCon('excusas');
  
  // preparamos el sql para ser ejecutado
  $excusas_sql = $db->prepare($sql);
  
  // si hay fecha de última excusa cargada
  if($fecha_ultima_excusa_cargada) {
  	
  	// atamos el parámetro de forma segura
  	$excusas_sql->bindParam('fecha_ultima_excusa_cargada', $fecha_ultima_excusa_cargada);
  	
  }
  
  // objeto que va a llenarse con cada excusa
  @$nueva_excusa = new Excusa();
  
  $excusas_sql->setFetchMode(PDO::FETCH_INTO, $nueva_excusa);
  
  $excusas_sql->execute();
  
  print '[';
  
  $i=0; $first = true;
  while($excusas_sql->fetch() && $i < 15) {
  	
  	if(!$first) {
  		
  		print ', ';
  		
  	}
  	
  	print $nueva_excusa;
  	
  	$first = false;
  	
  	$i++;
  	
  }
  
  print ']';

} catch(PDOException $e) {

  print $e->getMessage();

}
?>