<?
$tiempoInicio = microtime();

// creamos una variable para cada elemento enviado por POST
foreach($_POST as $Key => $value) {

	$$Key = utf8_decode($value);

}

// no perder tiempo si no hay cotenido en la excusa
if(!$contenido_excusa) {

	die('all ajax and no content makes script dead');

}

// incluímos la clase excusa
include("../php/MapObjects/excusa.php");

try {

	// creamos una nueva excusa
	$excusa = new Excusa(null);
	
	// llenamos la excusa nueva con los datos de Ajax
	// TODO esto se quita y se calcula dinámicamente
	$excusa->setCod_imp($cod_imp);
	
	// el tipo excusa debe ser permitido o defaults a txt
	if($tipo_excusa == 'img' || $tipo_excusa == 'vid') {
		
	  $excusa->setTipo_excusa($tipo_excusa);
	  
	  // TODO verificar que sea dirección web válida etc
	  $excusa->setContenido_excusa(trim($contenido_excusa));
	  
	}
	
	else {

	  $excusa->setTipo_excusa('txt');

	  // trimeamos y aplicamos entidades numéricas al contenido de usuario
	  $contenido_excusa_curado = htmlnumericentities(trim($contenido_excusa));
	  
	  // lo atrapamos entre un gran párrafo
	  $contenido_excusa_curado = '<p>'.$contenido_excusa_curado.'</p>';
	  
	  // y si tiene líneas nuevas creamos párrafos internos
	  $contenido_excusa_curado = preg_replace("/(&#10;)+/", '</p><p>', $contenido_excusa_curado);
	  
		$excusa->setContenido_excusa($contenido_excusa_curado);		
		
	}
	

	

	// la almacenamos en la base de datos y retornamos el número de 
	// filas afectadas en la operación
	$respuesta_json = $excusa->jr_creditar();
	
	// imprimimos el número de filas afectadas en la operación
	print $respuesta_json;

} catch(PDOException $e) {

	print $e->getMessage();

}
?>