/* class */ function Excusa (aExcusa_json) {

  this.cod_excusa;

  this.cod_imp;

  this.tipo_excusa;

  this.contenido_excusa;

  this.fecha_creacion_excusa;

  this.arrivotos;

  this.bajivotos;

  if(aExcusa_json) {

	this.setCodExcusa(aExcusa_json.cod_excusa);
	this.setCodImp(aExcusa_json.cod_imp);
	this.setTipoExcusa(aExcusa_json.tipo_excusa);
	this.setContenidoExcusa(aExcusa_json.contenido_excusa);
	this.setFechaCreacionExcusa(aExcusa_json.fecha_creacion_excusa);
	this.arrivotos = aExcusa_json.arrivotos;
	this.bajivotos = aExcusa_json.bajivotos;

  }

} Excusa.prototype = {

  jr_creditar : function(aCallback) {

	// enviamos al script los datos de la excusa nueva
	// TODO parametrizar esa dirección
	$.post('/excusa/ajax/creditarExcusa.json.php', {
	  cod_excusa : this.cod_excusa,
	  cod_imp : this.cod_imp,
	  tipo_excusa : this.tipo_excusa,
	  contenido_excusa : this.contenido_excusa
	}, function(excusa_json) {

	  aCallback(excusa_json);

	},
	'json');
  },

  setCodExcusa : function(aCod_excusa) {

    this.cod_excusa = aCod_excusa;

  },

  setCodImp : function(aCod_imp) {

    this.cod_imp = aCod_imp;

  },

  setTipoExcusa : function(aTipo_excusa) {

    this.tipo_excusa = aTipo_excusa;

  },

  setContenidoExcusa : function(aContenido_excisa) {

    this.contenido_excusa = aContenido_excisa;

  },

  setFechaCreacionExcusa : function(aFecha_creacion_excusa){

	  this.fecha_creacion_excusa = aFecha_creacion_excusa;

  },

  getCodExcusa: function() {

	   return this.cod_excusa;
  },

  getCodImp: function() {

    return this.cod_imp;

  },

  getTipoExcusa: function() {

    return this.tipo_excusa;

  },

  getContenidoExcusa: function() {

    return this.contenido_excusa;

  },

  getFechaCreacionExcusa : function(){

	  return this.fecha_creacion_excusa;

  },

  getArrivotos : function() {

	return this.arrivotos;

  },

  getBajivotos: function() {

	return this.bajivotos;

  },

  getPuntaje : function() {

	return (this.arrivotos - this.bajivotos);

  }

}