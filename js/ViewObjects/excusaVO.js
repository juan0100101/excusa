/* class */ function ExcusaVO(aData_object_excusa) {

  // almacenamos el DataObject de una excusa
  this.data_object_excusa;

  // el jElemento es de la forma i.imgur.com/L5EIl.png
  this.jElemento = $('<div class="contenedor_excusa"></div>');

  // si el constructor entrega una excusa
  if(aData_object_excusa) {

    this.setDataObjectExcusa(aData_object_excusa);

  }

} ExcusaVO.prototype = {

  actionBotonBajivoto : function () {

    // TODO acción de bajivotar una excusa
	alert('excusa con el id '+this.data_object_excusa.getCodExcusa()+' bajivotada');

  },

  actionBotonCompartir : function () {

    // TODO acción de compartir una excusa
	alert('excusa con el id '+this.data_object_excusa.getCodExcusa()+' compartida');

  },

  actionBotonArrivoto : function () {

    // TODO acción de arrivotar una excusa
	alert('excusa con el id '+this.data_object_excusa.getCodExcusa()+' arrivotada');

  },

  setDataObjectExcusa : function(aData_object_excusa) {

    this.data_object_excusa = aData_object_excusa;

    // limpiamos el jElemento
    this.jElemento.html('');

    var jContenedorCompartir = $('<div class="contenedor_compartir" />');

    var jBotonCompartir = $('<div class="compartir"><img src="img/compartir.png" width="20px" height="20px" /></div>');

    // TODO menú completo para compartir
    jContenedorCompartir.append(jBotonCompartir);

    // cremos los elementos visuales asociados al nuevo DataObject excusa
    var jAccionesExcusa = $('<div class="acciones_excusa" />');

      var jBotonBajivoto = $('<span class="boton_bajivoto" />');

        jBotonBajivoto.append($('<img src="img/bajivoto.png" width="20px" height="20px" />'));

      var jPuntaje = $('<span class="puntaje">'+this.data_object_excusa.getPuntaje()+'</span>');

      var jBotonArrivoto = $('<span class="boton_arrivoto" />');

        jBotonArrivoto.append('<img src="img/arrivoto.png" width="20px" height="20px" />');

    // almacenamos this porque vamos a cambiar de contexto
    var this_excusaVO = this;

    // clic en botón de bajivotar dispara su evento
    jBotonBajivoto.on('click', function(){

      this_excusaVO.actionBotonBajivoto();

    });

    // clic en botón de compartir dispara su evento
    jBotonCompartir.on('click', function(){

      this_excusaVO.actionBotonCompartir();

    });

    // clic en botón de arrivotar dispara su evento
    jBotonArrivoto.on('click', function(){

      this_excusaVO.actionBotonArrivoto();

    });

    jAccionesExcusa.append(jBotonBajivoto, jPuntaje, jBotonArrivoto);

    var jContenidoExcusa = $('<div />');

    if(this.data_object_excusa.getTipoExcusa() == 'img') {

      jContenidoExcusa.addClass('imagen');

      jContenidoExcusa.append($(''
      +'<div class="contenedor_imagen">'

        +'<img src="'+ this.data_object_excusa.getContenidoExcusa() +'" />'

        // TODO hacer que efectívamente clic en la imágen la abra en una pestaña nueva
        +'<p>'
          +'clic para ver en una nueva pestaña'
        +'</p>'

      +'</div>'));

    }

    else if(this.data_object_excusa.getTipoExcusa() == 'vid') {

	  jContenidoExcusa.addClass('imagen');

      jContenidoExcusa.append($(''
      +'<div class="contenedor_video">'

        +'<iframe src="'+this.data_object_excusa.getContenidoExcusa()+'" frameborder="0" allowfullscreen></iframe>'

      +'</div>'));

    }

    else {

      jContenidoExcusa.addClass('texto');

      jContenidoExcusa.html(this.data_object_excusa.getContenidoExcusa());

      // si la excusa es nueva (NOTA: una nueva siempre queda dentro de las buenas)
      if(this.getDataObjectExcusa().getArrivotos() == 0 &&
         this.getDataObjectExcusa().getBajivotos() == 0) {

    	// se le pone un link con un tag para ubicarla
    	this.jElemento.prepend('<a class="link_excusa_nueva" name="excusa_nueva"></a>');

      }

    }

    this.jElemento.append(jContenedorCompartir, jAccionesExcusa, jContenidoExcusa);

  },

  setJElemento : function(aJElemento) {

    this.jElemento = aJElemento;

  },

  getDataObjectExcusa: function() {

	   return this.data_object_excusa;
  },

  getJElemento: function() {

    return this.jElemento;

  }

}