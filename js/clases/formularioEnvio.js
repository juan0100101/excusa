/* class */ function FormularioEnvio () {
  
  this.tipo_excusa = 'txt';
  
  this.puedeEnviarse = false;
  
} FormularioEnvio.prototype = {
 
  setTipoExcusa : function(aTipo_excusa) {
	 
    this.tipo_excusa = aTipo_excusa;
	 
  },
  
  setPuedeEnviarse : function(aPuedeEnviarse) {
	  
	  this.puedeEnviarse = aPuedeEnviarse;
	  
  },
 
  getTipoExcusa : function() {
   
    return this.tipo_excusa;
  },
  
  isPuedeEnviarse : function() {
	  
	  return this.puedeEnviarse;
	  
  }
  
}