/* class */ function SeccionBuenasExcusas() {

  this.fecha_ultima_excusa_cargada = -1;

  this.jElementoSeccion;

  this.lista_excusasVO = new Array();

} SeccionBuenasExcusas.prototype = {

  /**
   * método para cargar 15 objetos <strong>excusaVO</strong> más a la sección, ordenados por su votación
   * <br><br>
   * @parameters:
   * <strong>function</strong> callback a llamar después de traer las nuevas excusas
   * <br><br>
   * @returns
   */
  agregar15Excusas : function(aCallback) {

	// almacenamos this porque vamos a cambiar de contexto
	var this_seccion_buenas_excusas = this;

	$.get('/excusa/ajax/cargarBuenasExcusas.json.php',{
	  fecha_ultima_excusa : this.fecha_ultima_excusa_cargada
	}, function(lista_excusas_json){

	  // sólo puede haber una excusa nueva resaltada
      var ya_hubo_resaltada = false;

	  // para cada excusa retornada
	  for(var i=0; i<lista_excusas_json.length; i++) {

	    // creamos un nuevo DataObject
		var nueva_excusa = new Excusa(lista_excusas_json[i]);

		// y un nuevo ViewObject
		var nuevo_excusaVO = new ExcusaVO(nueva_excusa);

		// si la excusa es nueva hay 20% de probabilidad de que quede de primera
		if(nuevo_excusaVO.getDataObjectExcusa().getArrivotos() == 0 &&
		   nuevo_excusaVO.getDataObjectExcusa().getBajivotos() == 0 &&
		   !ya_hubo_resaltada &&
		   Math.random() > 0.8
		) {

		  this_seccion_buenas_excusas.jElementoSeccion.prepend(nuevo_excusaVO.getJElemento());

		  this_seccion_buenas_excusas.lista_excusasVO.pop(nuevo_excusaVO);

		  this_seccion_buenas_excusas.fecha_ultima_excusa_cargada = nuevo_excusaVO.getDataObjectExcusa().getFechaCreacionExcusa();

		  // una excusa resaltada no se presenta como nueva para que
		  // queden enfocadas las recién enviadas
		  nuevo_excusaVO.getJElemento().find('a.link_excusa_nueva').remove();

		  // marcamos que ya hubo una excusa nueva resaltada
		  ya_hubo_resaltada = true;

		}

		else {

		  // como la lista ya viene ordenada, appendeamos el nuevo VO a la sección de excusas buenas
		  this_seccion_buenas_excusas.jElementoSeccion.append(nuevo_excusaVO.getJElemento());

		  // agregamos el VO a la lista
		  this_seccion_buenas_excusas.lista_excusasVO.push(nuevo_excusaVO);

		}
		// TODO remover y eliminar los primeros VOs de la lista después de determinada
		// cantidad de excusas cargadas


	  }

	  // ejecutamos función callback si fue pasada como parámetro
	  if(aCallback) {

        aCallback();

	  }

	},
	'json');

  },

  setJElementoSeccion : function(aJElementoSeccion) {

	this.jElementoSeccion = aJElementoSeccion;

  },

  getJElementoSeccion : function() {

	return this.jElementoSeccion;

  }

};