/* class */ function SeccionMalasExcusas() {

  this.fecha_ultima_excusa_cargada = -1;

  this.jElementoSeccion;

  this.lista_excusasVO;

} SeccionMalasExcusas.prototype = {

  // método para agregar nuevas excusas a la sección
  agregarASeccion : function(aCallback) {

	// almacenamos this porque vamos a cambiar de contexto
	var this_seccion_excusas = this;

	$.get('/excusa/ajax/cargarMalasExcusas.json.php',{
	  fecha_ultima_excusa : this.fecha_ultima_excusa_cargada
	}, function(lista_excusas_json){

	  // para cada excusa retornada
	  for(var i=0; i<lista_excusas_json.length; i++) {

	    // creamos una nueva instancia
		var nueva_excusa = new Excusa(lista_excusas_json[i]);

		// y un nuevo Visual Object
		var nuevo_excusaVO = new ExcusaVO(nueva_excusa);

		// como la lista ya viene ordenada, appendeamos el nuevo VO a la sección de excusas buenas
		this_seccion_excusas.jElementoSeccion.append(nuevo_excusaVO.getJElemento());

		// agregamos el VO a la lista

		// TODO remover y eliminar los primeros VOs de la lista después de determinada
		// cantidad de excusas cargadas


	  }

	  // ejecutamos el callback si fue pasado como parámetro
	  if(aCallback) {

        aCallback();

	  }

	},
	'json');

  },

  setJElementoSeccion : function(aJElementoSeccion) {

	this.jElementoSeccion = aJElementoSeccion;

  },

  getJElementoSeccion : function() {

	return this.jElementoSeccion;

  }

};