// objetos globales de control de la página
var formularioEnvio = new FormularioEnvio();
var seccionBuenasExcusas = new SeccionBuenasExcusas();
var seccionMalasExcusas = new SeccionMalasExcusas();

/**
 * crea un nuevo DataObject de Excusa, le asigna datos según indique el formulario
 * y finalmente lo persiste en la base de datos
 *
 */
function publicarExcusa() {

  // creamos una excusa nueva
  var nueva_excusa = new Excusa();

  // le asignamos los datos del formulario
  nueva_excusa.setCodImp(1);

  nueva_excusa.setTipoExcusa(formularioEnvio.getTipoExcusa());

  if(formularioEnvio.getTipoExcusa() == 'txt') {

    nueva_excusa.setContenidoExcusa(jTextarea.val());

  }

  else if(formularioEnvio.getTipoExcusa() == 'img') {

    nueva_excusa.setContenidoExcusa(jInputUrlImagen.val());

  }

  else if(formularioEnvio.getTipoExcusa() == 'vid') {

	    nueva_excusa.setContenidoExcusa(jInputVideo.val());

  }

  // la almancenamos en la base de datos
  nueva_excusa.jr_creditar(function(respuesta){

  // si el envío de la excusa fue correcto
  if(respuesta.numero_filas_agregadas == 1) {

    // recargamos la sección de excusas buenas y al terminar
    seccionBuenasExcusas.jElementoSeccion.html('');
    seccionBuenasExcusas.agregarASeccion(function(){

      // enfocamos la primera excusa nueva
      document.location.href = '#excusa_nueva';

    });





    }

    else {

	  alert('hubo un problema agregando su excusa a la base de datos');

    }

  });

}

/**
 * método tomado de http://api.imgur.com/examples#uploading_js noviembre 2011
 */
function subirImagenAImgur(archivo) {

  // archivo viene del atributo .file de un <input> de tipo file
  // TODO recibir archivos arrastrados. Debe ser trivial.
  // Ver ejemplo en http://paulrouget.github.com/miniuploader/

  // construimos un objeto FormData
  var formData = new FormData();

  // Append the file
  formData.append('image', archivo);

  // clave fue conseguida en http://api.imgur.com/
  formData.append('key', '2d325478365458bcdeaa8999d53168e1');

  // crear petición XMLHttp (Cross-Domain gracias a Alan de Imgur :)
  var peticionXMLHttp = new XMLHttpRequest();

  peticionXMLHttp.open('POST', 'http://api.imgur.com/2/upload.json');

  // función que se dispara al llegar la petición
  peticionXMLHttp.onload = function() {

	var image_json = JSON.parse(peticionXMLHttp.responseText);

	// si el llamado Ajax salió bien y Imgur contestó con algo
	if(peticionXMLHttp.readyState == 4 && image_json.upload.links.imgur_page != undefined) {

	  // la imagen queda lista para ser publicada
	  jInputUrlImagen.val(image_json.upload.links.original);

	  // TODO capturar mucha más información de la imágen que retorna Imgur
	  // para enviarla a la base de datos

	  // activamos evento que revisa si hay algo en el input de URL de la imagen
	  validarMostrarBotonPublicar(jInputUrlImagen);

	  // cargamos la imágen para que el usuario confirme
	  jImagenSubida.attr('src', image_json.upload.links.large_thumbnail);

	  // mostramos el área de confirmación de imagen
	  jContenedorImagenSubida.removeClass('_oculto _transparente');

	  // detenemos la animación de loading
	  jContenedorFormularioImagen.trigger('desprocesar');

	  // pero no mostramos el forumario de nuevo
	  jContenedorFormularioImagen.addClass('_oculto');

	  // TODO mostramos botón para que el usuario pueda indicar
      // que la imagen es errónea y que quiere subir otra imágen


	}

	else {

	  // volvemos a mostrar el formulario para subir una imágen
	  // TODO este comportamiento no es el apropiado para el caso de uso
	  jContenedorFormularioImagen.trigger('desprocesar');

	}

  }

  // Ok, I don't handle the errors. An exercice for the reader.
  // And now, we send the formdata
  peticionXMLHttp.send(formData);

}

function validarMostrarBotonPublicar(aJInput) {

  if(aJInput.val() != '') {

    jBotonPublicar.addClass('activo');

    formularioEnvio.setPuedeEnviarse(true);

  }

  else {

    jBotonPublicar.removeClass('activo');

    formularioEnvio.setPuedeEnviarse(false);

  }

}

$(document).ready(function() {

  //$('hmtl, body').animate({scrollTop: $(document).height()-200}, 0);

  jBotonTexto = $('#boton_texto');
  jTextarea = $('#textarea');

  jBotonImagen = $('#boton_imagen');
  jContenedorFormularioImagen = $('#contenedor_formulario_imagen');
  jInputUrlImagen = $('#input_url_imagen');
  jInputFile = $('#input_file');
  jImagenSubir = $('#imagen_subir');
  jContenedorImagenSubida = $('#contenedor_imagen_subida');
  jImagenSubida = $('#imagen_subida');

  jBotonVideo = $('#boton_video');
  jContenedorFormularioVideo = $('#contenedor_formulario_video');
  jInputVideo = $('#input_video');
  jLinkYoutube = $('#link_youtube');
  jLinkVimeo = $('#link_vimeo');

  jBotonPublicar = $('#boton_publicar');

  jContenedorBuenasExcusas = $('#contenedor_seccion_buenas_excusas');
  jBotonCargarMasBuenasExcusas = $('#boton_cargar_mas_buenas_excusas');

  jContenedorMalasExcusas = $('#contenedor_seccion_malas_excusas');

  jLinkExcusaNueva = $('#link_excusa_nueva');

  jAnimal = $('#animal');
  jFooter = $('#footer');



  /** acciones justo alcargar la página **/

  seccionBuenasExcusas.setJElementoSeccion(jContenedorBuenasExcusas);
  seccionMalasExcusas.setJElementoSeccion(jContenedorMalasExcusas);

  seccionBuenasExcusas.agregar15Excusas();
  seccionMalasExcusas.agregarASeccion();



  /** listeners **/

  // cuando hay clic en botón de publicar
  jBotonPublicar.click(function(){

	// si el formulario está activo para envío
	if(formularioEnvio.isPuedeEnviarse()) {

	  publicarExcusa();

	}

  });


  // cuando hay clic en imagen de subir una foto
  jImagenSubir.click(function(){

	  // activamos el botón del input de subir archivo
	  jInputFile.trigger('click');

  });

  // y cuando hay un archivo escogido
  jInputFile.change(function(){

	  setTimeout(function(){

		// intentamos enviarlo a Imgur
	    subirImagenAImgur(jInputFile.get(0).files[0]);

	  }, 100);

	  jContenedorFormularioImagen.trigger('procesar');

  });



  // esto precarga la imágen
  jAnimal.attr('src','img/animal.png');

  setTimeout(function(){

	  jAnimal.attr('src','img/animal_colorless.png');

  }, 100);

  // cuando hay clic en el animal
  jAnimal.click(function(){

	  // se muestran el animal a color
	  jAnimal.attr('src','img/animal.png');

	  // y el footer
	  jFooter.removeClass('_oculto');

	  setTimeout(function(){

	    jFooter.removeClass('_transparente');

	  }, 10);

	  $('hmtl, body').animate({scrollTop: $(document).height()-200}, 700);

  });



  jTextarea.keyup(function(){

    validarMostrarBotonPublicar(jTextarea);

  });

  jInputUrlImagen.keyup(function(){

    validarMostrarBotonPublicar(jInputUrlImagen);

  });

  jInputVideo.keyup(function(){

    validarMostrarBotonPublicar(jInputVideo);

  });

  // click en botón de enviar excusa de texto
  jBotonTexto.click(function() {

    formularioEnvio.setTipoExcusa('txt');

    validarMostrarBotonPublicar(jTextarea);

    // oculta inputs para otros
    jContenedorFormularioImagen.addClass('_transparente');
    jContenedorFormularioVideo.addClass('_transparente');

    setTimeout(function() {

      // muestra textarea
      jTextarea.removeClass('_oculto');

      jContenedorFormularioImagen.addClass('_oculto');
      jContenedorFormularioVideo.addClass('_oculto');

    }, 500);

    setTimeout(function(){

      jTextarea.removeClass('_transparente');

    }, 510);


    // resalta sólo botón de texto
    jBotonTexto.addClass('seleccionado');
    jBotonImagen.removeClass('seleccionado');
    jBotonVideo.removeClass('seleccionado');

  });

  // click en botón de enviar imagen
  jBotonImagen.click(function() {

    formularioEnvio.setTipoExcusa('img');

    validarMostrarBotonPublicar(jInputUrlImagen);

    // oculta inputs para otros
    jTextarea.addClass('_transparente');
    jContenedorFormularioVideo.addClass('_transparente');

    setTimeout(function() {

      // muestra formulario para enviar imagen
      jContenedorFormularioImagen.removeClass('_oculto');

      jTextarea.addClass('_oculto');
      jContenedorFormularioVideo.addClass('_oculto');

    }, 500);

    setTimeout(function() {

      // muestra formulario para enviar imagen
      jContenedorFormularioImagen.removeClass('_transparente');

    }, 510);

    // resalta sólo botón de imagen
    jBotonTexto.removeClass('seleccionado');
    jBotonImagen.addClass('seleccionado');
    jBotonVideo.removeClass('seleccionado');

  });

  jBotonVideo.click(function() {

    formularioEnvio.setTipoExcusa('vid');

    validarMostrarBotonPublicar(jInputVideo);

    // oculta inputs para otros
    jTextarea.addClass('_transparente');
    jContenedorFormularioImagen.addClass('_transparente');

    setTimeout(function() {

      // muestra formulario para enviar video
      jContenedorFormularioVideo.removeClass('_oculto');

      jTextarea.addClass('_oculto');
      jContenedorFormularioImagen.addClass('_oculto');

    }, 500);

    setTimeout(function() {

      // muestra formulario para enviar video
      jContenedorFormularioVideo.removeClass('_transparente');

    }, 510);

    // resalta sólo botón de video
    jBotonTexto.removeClass('seleccionado');
    jBotonImagen.removeClass('seleccionado');
    jBotonVideo.addClass('seleccionado');

  });

  //click en link de youtube
  jLinkYoutube.click(function(){

    // va a imgur
    window.open('https://accounts.google.com/ServiceLogin?uilel=3&service=youtube&passive=true&continue=http%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26nomobiletemp%3D1%26hl%3Des_ES%26next%3Dhttp%253A%252F%252Fwww.youtube.com%252Fmy_videos_upload&hl=es_ES&ltmpl=sso');

  });

  // click en link de vimeo
  jLinkVimeo.click(function(){

    // va a imgur
    window.open('http://vimeo.com');

  });

  jBotonCargarMasBuenasExcusas.click(function(){

	  seccionBuenasExcusas.agregar15Excusas();

  });

});