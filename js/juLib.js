/* ubica un elemento de 'loading' como primer hijo del elemento
 * recibido como argumento y esconde todos los demás.
 */
function procesarJElemento(aJElemento) {

  j_AutoProcesable_Origen.removeClass('_oculto');

  setTimeout(function(){

	j_AutoProcesable_Origen.removeClass('_transparente _fantasma');

  }, 10)

  aJElemento.children().hide();

}

/* oculta el elemento 'loading' en donde quiera que está, y muestra
 * todos los elementos hijos del elemento recibido como argumento
 */
function desprocesarJElemento(aJElemento) {

  j_AutoProcesable_Origen.addClass('_transparente _fantasma');

  setTimeout(function(){

	j_AutoProcesable_Origen.addClass('_oculto');

	aJElemento.children().show();

  }, 1000)
}

$(document).ready(function() {

  // agregamos reglas de propósito general
  jQuery('<style type="text/css"> '

    +'@import url(http://fonts.googleapis.com/css?family=Lato:300,400);'

    +'._transparente {'
      +'opacity: 0;'

      +'transition: opacity 1s ease-in;'
      +'-moz-transition: opacity 1s ease-in;'
      +'-webkit-transition: opacity 1s ease-in;'
    +'}'

    +'._oculto {'
      +'display: none;'
    +'}'

    +'._fantasma {'
      +'position: absolute;'
      +'z-index: 196;'
    +'}'

  +'._contenedor_procesando {'
    +'padding: 20px;'
    +'font-size: 16px;'
    +'text-align: center;'
  +'}'

  +'</style>').appendTo('head');

  // buscamos un loading
  j_AutoProcesable_Origen = $('._autoProcesable_origen');

  // si no hay creamos uno
  if(j_AutoProcesable_Origen.length == 0) {

	j_AutoProcesable_Origen = ('<div class="_contenedor_procesando _oculto _transparente _fantasma" />').text('cargando');

  }

  // asignamos evento procesar a todos los elementos procesables
  $('._autoProcesable').bind('procesar', function(){

        procesarJElemento($(this));

  });

  // asignar evento desprocesar a todos los elementos procesables
  $('._autoProcesable').bind('desprocesar', function(){

        desprocesarJElemento($(this));

  });

})