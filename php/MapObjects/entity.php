<?

include('../php/conexion_db.php');

/*
 * entity es la clase de la que todas las clases que mapean datos se
 * derivan. Se encarga también de conectarlas a la base de datos y de
 * su impresión para entregarlas a scrips de Ajax ya sea como JSON o
 * en notación XML
*/
class Entity {

// objeto PDO de conexión a base de datos
protected $db;

// la función del juanager de connectar puede estar aquí, porque es
// igual para todas las clases que mapean entidades. Sólo recibe la
// base de datos en la que se encuentra la entidad
protected function jr_connectar($database) {

	// creamos objeto PDO conectado a base de datos indicada por $database
  try {
          
  // y lo almacenamos como propiedad del objeto
  $this->db = conectarCon($database);
  
  } catch(PDOException $e) {
  
    throw new PDOException($e);
          
  }

}

/* TODO guardar el nombre del esquema en la entidad y pensar en cuánto más
 es posible parametrizar este mapeador. Es posible hacer los métodos de llenado?
 quizá con información sobre cómo se llaman las primary keys y qué tipo de datos
 son? y recibiendo como parámetros los demás datos que van a pedirse?
 (no es trivial: las relaciones entre tablas hacen necesario hacer JOINs, etc.
 en esos casos yo necesitaría crear entidades con sentencias SQL muy particulares)
 La salida a la complejidad es tan sencilla como traer otra entidad con el nombre
 indicado y usarla como una propiedad? hay recursión infinita en este caso cuando
 hay tablas con vínculos triangulares etc?
 */

// constructor
private function __construct() {
}

/** métodos para impresión de la entity como JSON, o XML si se necesitara en el futuro **/
// los siguientes métodos salieron de
// http://www.php.net/manual/es/function.json-encode.php#98718
public function toArray() {

    return $this->processArray(get_object_vars($this));

}

private function processArray($array) {

    foreach($array as $key => $value) {
        
        // esto falló en dic 2011; es obligatorio que el objeto tenga método toArray
    	  if (is_object($value) && method_exists($value, 'toArray')) {
        
            $array[$key] = $value->toArray();
            
        }
        
        if (is_array($value)) {
        
            $array[$key] = $this->processArray($value);
            
        }
        
    }
    
    // If the property isn't an object or array, leave it untouched
    return $array;
}

public function __toString() {

  // dic 2011: es necesario des-escapar los slashes que json_encode
  // escapa por defecto porque así lo decidieron las lumbreras de PHP
  // json_encode($this->toArray())
	return str_replace('\\/', '/', json_encode($this->toArray()));
    
}
}
?>