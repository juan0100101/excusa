<?
// incluímos la clase base para el mapeo de entidades
include ('entity.php');

/*
* una excusa es un texto. Un tipo de excusa indica si el texto
* debe ser tratado de forma literal, o interpretado como el URL
* de una imagen, o como el URL de un video de Internet. Algunos
* de esos URLs pueden apuntar a este mismo servidor
*/
class Excusa extends Entity {

// PK TODO hacer estos también privados
public $cod_excusa;

// FK
public $cod_imp;

// determina si es un texto, imagen o video
public $tipo_excusa;

// el texto, o URL de la imagen o video
public $contenido_excusa;

// los votos positivos asociados
public $arrivotos;

// los votos negativos asociados
public $bajivotos;




/** variables y métodos del juanager **/

private $esquema;

/*
 * función del juanager que llena un objeto, buscando uno con
 * su código en la base de datos 
 */
public function jr_llenar() {

	// si no hay llave primaria se lanza una excepción
	if(!$this->cod_excusa) {

		throw new Exception('jr::imposible llenar una excusa sin su lave primaria<br />');

	}

	$this->jr_connectar($this->esquema);

	// buscamos los datos de una excusa
	$sql= 'SELECT cod_imp, '
	            .'tipo_excusa, '
	            .'contenido_excusa, '
	            .'fecha_creacion_excusa, '
	            .'(SELECT COUNT(*) FROM excusas.votos WHERE votos.cod_excusa = excusas.cod_excusa AND votos.calificacion_voto = \'p\') AS arrivotos, '
	            .'(SELECT COUNT(*) FROM excusas.votos WHERE votos.cod_excusa = excusas.cod_excusa AND votos.calificacion_voto = \'n\') AS bajivotos '
	     .'FROM excusas '
	     .'WHERE cod_excusa = :cod_excusa';

	$excusas_sql = $this->db->prepare($sql);

	// que tenga el código de este objeto 
	$excusas_sql->bindParam('cod_excusa', $this->cod_excusa);

	// nos preparamos para volcar lo encontrado en este mismo objeto
	$excusas_sql->setFetchMode(PDO::FETCH_INTO, $this);

	// si hay alguna excusa con el código
	if($excusas_sql->execute()) {

		// buscarla e insertarla al objeto
		$excusa = $excusas_sql->fetch();

	}

	$this->db = null;

}

/*
 * función que crea una entrada en la base de datos con este objeto
 * TODO o edita una ya existente si tiene un código y encuentra una entrada
 * en la tabla con ese código
 * */
public function jr_creditar() {
	
	$this->jr_connectar($this->esquema);

	// si el objeto no tiene código sabemos que hay que crear
	// una entrada nueva
	if(!$this->cod_excusa) {

		$sql = 'INSERT INTO excusas '
		      .'(cod_imp, tipo_excusa, contenido_excusa, fecha_creacion_excusa) '
		      .'VALUES '
		      .'(:cod_imp, :tipo_excusa, :contenido_excusa, '.time().')';

		$nueva_excusa_sql = $this->db->prepare($sql);

		// bindeamos propiedades de la clase
		$nueva_excusa_sql->bindParam('cod_imp', $this->cod_imp);
		$nueva_excusa_sql->bindParam('tipo_excusa', $this->tipo_excusa);
		$nueva_excusa_sql->bindParam('contenido_excusa', $this->contenido_excusa);
		
		$nueva_excusa_sql->execute();
		
		return '{"numero_filas_agregadas":"'.$nueva_excusa_sql->rowCount().'"}';
		
		// terminamos la conexión
		$this->db = null;
		
	}
	
}

// constructor
public function __construct($codExcusa) {

	$this->cod_excusa = $codExcusa;
	
	$this->esquema = 'excusas';

}

/** accessors **/
public function getCod_excusa() {
    return $this->cod_excusa;
}

public function setCod_excusa($cod_excusa) {
    $this->cod_excusa = $cod_excusa;
}

public function getCod_imp() {
    return $this->cod_imp;
}

public function setCod_imp($cod_imp) {
    $this->cod_imp = $cod_imp;
}

public function getTipo_excusa() {
    return $this->tipo_excusa;
}

public function setTipo_excusa($tipo_excusa) {
    $this->tipo_excusa = $tipo_excusa;
}

public function getContenido_excusa() {
    return $this->contenido_excusa;
}

public function setContenido_excusa($contenido_excusa) {
    $this->contenido_excusa = $contenido_excusa;
}
}
?>